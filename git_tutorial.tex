\documentclass{beamer}

\mode<presentation> {
\usetheme{Madrid}
\usecolortheme{beaver}
\setbeamertemplate{footline}[page number]
\setbeamertemplate{navigation symbols}{}
}

\usepackage{graphicx}
\usepackage{booktabs} % Allows use of \top/mid/bottomrule in tables
\usepackage{hyperref}
% \usepackage{listings}

%-----------------------------------------------
%   TITLE PAGE
%-----------------------------------------------

\title[Git Tutorial]{Version Control using Git}

\author{Arunkumar Bupathy}
\institute{
\textit{arunbupathy@gmail.com}
}
\date{\today}

\begin{document}

\begin{frame}
\titlepage % Print the title page as the first
\end{frame}

\begin{frame}
\frametitle{Outline}
\tableofcontents
\end{frame}

\section{What is Version Control and Why?}

\begin{frame}
\frametitle{What is Version Control}

\begin{itemize}
\item {\color{darkred} Version control = managing versions of projects and tracking changes}.
\item You probably do some version control - folder names, dates, etc.
\begin{figure}
\centering
\includegraphics[width=0.4\linewidth]{fig/real_project.png}
\label{fig:messy_project}
\end{figure}
\item {\color{darkred} Error prone (accidental overwriting / deletion) and confusing.}
\item ``git'' aids version control - track, label, compare, revert and more.
\item Importantly, your projects are tidy and manageable.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Initial Set Up}

\begin{itemize}
\item Once we have git installed, we need to set up some basic stuff.
\item In a terminal, type (without the \$):
{\color{teal} \begin{verbatim}
$ git config --global user.name "yourusername"
$ git config --global user.email "your_id@example.com"
$ git config --global core.editor kate
$ git config --global diff.tool kdiff3
\end{verbatim}}
\item You are free to use your editor and difftool of choice.
\end{itemize}
\end{frame}

\section{Creating a Repo, Tracking Changes}

\begin{frame}[fragile]
\frametitle{First Git Project}

\begin{itemize}
\item Let us make a new project folder and create a readme file:
{\color{teal} \begin{verbatim}
$ mkdir first_git_project && cd first_git_project
$ echo "My first git project" > readme.txt
\end{verbatim}}
\item Initialize git in the project folder and check status:
{\color{teal}
\begin{verbatim}
$ git init
$ git status
\end{verbatim}}
\item What does it say?
\item {\color{darkred} Tell git to track changes to readme.txt:}
{\color{teal}
\begin{verbatim}
$ git add readme.txt
$ git status
\end{verbatim}}
\item {\color{darkred}\verb|git add --all|} adds all the files in the current folder.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Commits and History}

\begin{itemize}
\item To create a snapshot of the tracked file do:
{\color{teal}\begin{verbatim}
$ git commit -m "Added readme."
\end{verbatim}}
\item {\color{darkred} Commit often, one change at a time, and describe in the message!}
\item git log prints the history of commits (in the current branch).
{\color{teal}\begin{verbatim}
$ git log
\end{verbatim}}
\item It is too verbose, so let us create a shorter version:
{\color{teal}\begin{verbatim}
$ git config --global alias.ph\
            'log --pretty=format:"%h - %an, %ar: %s"'
$ git ph
\end{verbatim}}
\item Now, that is short and sweet.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Tracking Changes}

\begin{itemize}
\item Git is all about tracking changes. So let us edit the readme.txt:
{\color{teal} \begin{verbatim}
$ echo "This project has no files." >> readme.txt
$ git status
\end{verbatim}}
\item Git should tell you that the file has been modified.
\item To take a snapshot of the file in its current state do:
{\color{teal} \begin{verbatim}
$ git add readme.txt
\end{verbatim}}
\item {\color{darkred}\verb|git add|} tells that readme.txt is to be ``staged'' for committing.
\item {\color{darkred} Note: changes that are not staged will not be committed!}
{\color{teal} \begin{verbatim}
$ git commit -m "Updated file count in readme."
\end{verbatim}}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Why Staging?}

\begin{itemize}
\item Let us add a new file to the project.
{\small \color{teal}
\begin{verbatim}
$ echo "I love coffee!" > coffee.txt
$ git add --all
\end{verbatim}}
\item Staging is useful to temporarily save changes without commiting.
\item You can now edit coffee.txt:
{\small \color{teal}
\begin{verbatim}
$ echo "Coffee is better than tea." >> coffee.txt
$ git status
\end{verbatim}}
\item But if you changed your mind, you can revert to the staged version:
{\small \color{teal}
\begin{verbatim}
$ git restore cofee.txt
$ git commit -m "Added coffee.txt."
\end{verbatim}}
\item In fact, you can even revert staged changes.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{How Git Does It}

\begin{itemize}
\item Git stores snapshots in a special folder called \verb|.git| within your project.
\item This is hidden away from view so that things are neat and tidy.
\begin{figure}
\centering
\includegraphics[width=0.67\linewidth]{fig/git_internals.pdf}
\label{fig:git_internals}
\end{figure}
\item \verb|master| is the default branch. It could have been called anything else.
\item \verb|HEAD| can point to anything. A copy of that is what you can see / edit.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Rolling Back a File to an Earlier State}

\begin{itemize}
\item Find the commit ``hash'' of the required version (say using \verb|git log|)
\item Use difftool to check if this is the right version:
{\color{teal}
\begin{verbatim}
$ git difftool <commit-hash> HEAD -- readme.txt
\end{verbatim}}
\item If correct, then {\color{darkred} check out} that file:
{\color{teal}
\begin{verbatim}
$ git checkout <commit-hash> -- readme.txt
\end{verbatim}}
\item This file is automatically staged so you can commit it:
{\color{teal}{\small
\begin{verbatim}
$ git commit -m "Removed (reverted) file count from readme."
\end{verbatim}}}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Rolling Back the Whole State}

\begin{itemize}
\item Do {\small \color{darkred}\verb|git checkout <commit-hash>|} to temporarily ``switch'' to an earlier state.
\item Make sure that this is the state you want to revert to.
\item Go back to the tip of the current branch (master, in this case):
{\small \color{teal}
\begin{verbatim}
$ git checkout master
\end{verbatim}}
\item To revert to that state, do:
{\small \color{teal}
\begin{verbatim}
$ git revert <commit-hash>
\end{verbatim}}
\item The given commit is applied as a change on top of current branch.
\item This prevents accidentally deleting files from the history.
\end{itemize}
\end{frame}

\section{Remote Repos, File Conflicts, Branches}

\begin{frame}[fragile]
\frametitle{Remote Repository}

\begin{itemize}
\item We can set up a central sync point or backup for our repository.
\item It can be on our local disk (or a web host like github.com).
\item Make a folder (with a \verb|.git| suffix), say, on a usb drive:
{\small \color{teal}
\begin{verbatim}
$ mkdir first_git_project.git && cd first_git_project.git
$ git init --bare
\end{verbatim}}
\item Now navigate back to the project folder and do:
{\small \color{teal}
\begin{verbatim}
$ git remote add origin /path/to/remote/first_git_project.git
\end{verbatim}}
\item \verb|origin| is a name for the remote. Now we can {\color{darkred}push} to the remote:
{\small \color{teal}
\begin{verbatim}
$ git push origin master
\end{verbatim}}
\item {\color{darkred} Use separate remote for each project, and do not edit it manually.}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Cloning and Pulling from Remote}

\begin{itemize}
\item We can now {\color{darkred}clone} from the remote, say, on your office desktop:
{\small \color{teal}
\begin{verbatim}
$ git clone /path/to/remote/repo/first_git_project.git
\end{verbatim}}
\item Say you edited the readme at office and pushed it to the remote:
{\small \color{teal}
\begin{verbatim}
$ echo "Working from office." >> readme.txt
$ git add readme.txt
$ git commit -m "Updated readme from office."
$ git push origin master
\end{verbatim}}
\item You can {\color{darkred}pull} this change on your laptop by doing:
{\small \color{teal}
\begin{verbatim}
$ git pull origin
\end{verbatim}}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Merge Conflicts}

\begin{itemize}
\item Merge conflicts happen when a file is edited parallelly in two locations.
\item Simulate a conflict by editing readme in our two copies of the repo.
\item Commit the changes in both places, and try to push them. On the second location, git would have failed.
\end{itemize}
\begin{figure}
\centering
\includegraphics[width=0.5\linewidth]{fig/divergence.png}
\label{fig:conflict}
\end{figure}

\end{frame}

\begin{frame}[fragile]
\frametitle{Resolving Conflicts}

\begin{itemize}
\item As git suggests, let us pull the changes:
{\small \color{teal}
\begin{verbatim}
$ git pull
\end{verbatim}}
\item This should open the conflicting file, with text from both versions.
\item Edit the file the way you want it and save it.
\item Don't forget to remove \verb|<<<, ===, >>>, HEAD, origin/master,|.
\item Stage the file, commit it and push.
\begin{figure}
\centering
\includegraphics[width=0.5\linewidth]{fig/resolution.png}
\label{fig:resolution}
\end{figure}

\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Branches}

\begin{itemize}
\item Sometimes you do not want to work on the master branch.
\begin{figure}
\centering
\includegraphics[width=0.6\linewidth]{fig/branches.png}
\label{fig:branches}
\end{figure}
\item Let us {\color{darkred} create a branch} called \verb|experimental| {\color{darkred}and switch} to it:
{\small \color{teal}
\begin{verbatim}
$ git branch experimental
$ git checkout experimental
\end{verbatim}}
\item Now you can continue editing and commiting files to \verb|experimental|:
{\small
\begin{verbatim}
$ echo "Is this better?" >> readme.txt
$ git add readme.txt && git commit -m "An experimental edit."
\end{verbatim}}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Merging Branches}

\begin{itemize}
\item Once you are satisfied with your experiments, you can merge these to master.
\item {\color{darkred}Make sure you are on the master branch by switching to it!}
{\small \color{teal}
\begin{verbatim}
$ git checkout master
$ git merge experimental
\end{verbatim}}
\item Resolve merge conflicts if any, as usual.
\begin{figure}
\centering
\includegraphics[width=0.67\linewidth]{fig/branch_merge.png}
\label{fig:merging}
\end{figure}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Thank you}

\begin{itemize}
\item Things not discussed: GUI tools, tags, rebasing, fetch-merge, ``blame'' and collaborating!
\item Now that you know the basics, you can follow tutorials online.
\item Version control is something you need to experience for yourself.
\end{itemize}

\vspace{2ex}
\begin{center}
{\color{darkred}
Thanks for your patience!}
\end{center}

\vspace{3ex}
\small
{\color{teal} I would like to thank Prof. Stefano Allesina, Dept.~of Ecology and Evolution, University of Chicago, for his course on computational tools for scientists, where I learned some of this.}
\end{frame}

\end{document}
